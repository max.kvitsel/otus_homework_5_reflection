﻿### Reflection
*Сериализуемый класс:* class F { int i1, i2, i3, i4, i5;}
#### Результаты:

 Iterations | Method | Time, ms 
--- | --- | --- 
1000000 | SerializeToCSV | 1698
1000000 | DeserializeFromCSV | 111
1000000 | Newtonsoft.Json.JsonConvert.SerializeObject | 1325
1000000 | Newtonsoft.Json.JsonConvert.DeserializeObject | 407