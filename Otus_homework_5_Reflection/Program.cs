﻿
using System;
using System.Diagnostics;
using Newtonsoft.Json;


namespace Otus_homework_5_Reflection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            Serializer serializer = new Serializer();
            F fClassObject = F.Get();
            int iterations = 1000000;

            string rows = String.Empty;
            int count = 0;
            timer.Start();
            while (count < iterations)
            {
                rows = serializer.SerializeToCSV(fClassObject);
                count++;
            }
            timer.Stop();
            Console.WriteLine($"SerializeToCSV result:\n{rows}");
            Console.WriteLine($"Time(ms): {timer.ElapsedMilliseconds}");

            Console.WriteLine("---");

            timer = new Stopwatch();
            rows = String.Empty;
            count = 0;
            timer.Start();
            while (count < iterations)
            {
                fClassObject = serializer.DeserializeFromCSV<F>(rows);
                count++;
            }
            timer.Stop();
            Console.WriteLine("DeserializeFromCSV time(ms): " + timer.ElapsedMilliseconds);

            Console.WriteLine("---");

            timer = new Stopwatch();
            rows = String.Empty;
            count = 0;

            timer.Start();
            while (count < iterations)
            {
                rows = JsonConvert.SerializeObject(fClassObject);
                count++;
            }
            timer.Stop();
            Console.WriteLine($"Json serialization result: {rows}");
            Console.WriteLine($"Time(ms): {timer.ElapsedMilliseconds}");

            Console.WriteLine("---");

            timer = new Stopwatch();
            rows = String.Empty;
            count = 0;

            timer.Start();
            while (count < iterations)
            {
                fClassObject = JsonConvert.DeserializeObject<F>(rows);
                count++;
            }
            timer.Stop();
            Console.WriteLine("Json deserialization time(ms): " + timer.ElapsedMilliseconds);

        }

    }
}
