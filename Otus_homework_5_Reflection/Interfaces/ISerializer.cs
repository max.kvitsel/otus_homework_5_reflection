﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_homework_5_Reflection.Interfaces
{
    public interface ISerializer
    {
        public string SerializeToCSV(object obj);
        public T DeserializeFromCSV<T>(string csv) where T : class, new();

    }
}
